package hello;


import java.math.BigInteger;
import java.util.Random;
import java.security.MessageDigest;

public class Crypto {
    private String                  clientUrl;
    private String                  clientKey;
    private String                  completeKey;
    private String                  completeHash;
    private Integer                 clientLength;
    private Random                  r;
    private long                    seed;
    private boolean                 withSpeChar;
    static private char             listSpeChar[] = { '!', '#', '%', '&', '(', ')', '*', '+', ',', '.', '/', '~',
                                                  ':', ';', '<', '=', '>', '?', '@', '[', ']', '^', '_', '`', '{', '|',
                                                  '}' };
    static private char             listMaj[]     = { 'A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'Q', 'S', 'D',
                                                  'F', 'G', 'H', 'J', 'K', 'L', 'M', 'W', 'X', 'C', 'V', 'B', 'N' };

    public Crypto( String clientUrl, String clientKey, Boolean withSpeChar, Integer clientLength ) {

            this.clientUrl = clientUrl;
            this.clientKey = clientKey;
            this.withSpeChar = withSpeChar;
            this.clientLength = clientLength;
            completeKey = clientUrl + "n" + clientKey + "a" + withSpeChar + "t" + clientLength + "s";
            hashGener();
            setSeed();
            r = new Random( seed );
    }

    public String getPwd() {
        return ( addMaj( withSpeChar ? addSpeChar( completeHash ) : completeHash ).substring(0,clientLength) );
    }

    private void hashGener() {
        try {
            byte[] bytesOfMessage = completeKey.getBytes( "UTF-8" );
            MessageDigest md = MessageDigest.getInstance( "SHA-512" );
            byte[] thedigest = md.digest( bytesOfMessage );
            BigInteger bigInt = new BigInteger( 1, thedigest );
            completeHash = bigInt.toString( 32 );
        } catch ( Exception e ) {
        }
    }

    private void setSeed() {
        seed = Long.parseLong( completeHash.replaceAll( "[^\\d]", "" ).substring( 0, 10 ) );
    }

    private String addMaj( String str ) {
        return incruste( str, getSuite( getNb(), 0, clientLength - 1 ), listMaj );
    }

    private String addSpeChar( String str ) {
        return incruste( str, getSuite( getNb(), 0, clientLength - 1 ), listSpeChar );
    }

    private int[] getSuite( int nb, int min, int max ) {
        int[] tab = new int[nb];
        for ( int i = 0; i < nb; i++ ) {
            tab[i] = getRand( r, min, max );
        }
        return tab;
    }

    private int getNb() {
        return getRand( r, clientLength * 2 / 10, clientLength * 4 / 10 );
    }

    private String incruste( String str, int[] intTab, char[] liste ) {
        for ( Integer i : intTab ) {
            str = replaceCharAt( str, i, liste[getRand( r, 0, liste.length - 1 )] );
        }
        return str;
    }

    private String replaceCharAt( String s, int pos, char c ) {
        return s.substring( 0, pos ) + c + s.substring( pos + 1 );
    }

    private int getRand( Random r, int low, int high ) {
        return r.nextInt( high - low ) + low;
    }
}