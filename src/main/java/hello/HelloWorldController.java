package hello;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/hello-world")
    @ResponseBody
    public Greeting sayHello(@RequestParam(name="name", required=false, defaultValue="Stranger") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }
    
    @GetMapping("/")
    @ResponseBody
    public Password sayPassword(
	      @RequestParam(name="clientUrl", required=true) String clientUrl
	    , @RequestParam(name="clientKey", required=true) String clientKey
	    , @RequestParam(name="withSpeChar", required=false, defaultValue="false") boolean withSpeChar
	    , @RequestParam(name="clientLenght", required=false, defaultValue="16") int clientLenght

	    ) {
        return new Password(clientUrl, clientKey, withSpeChar, clientLenght);
    }

}
