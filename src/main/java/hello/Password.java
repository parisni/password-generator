package hello;

public class Password {

    private String password;

    public Password(String clientUrl, String clientKey, boolean withSpeChar, int clientLenght) {
	this.password = (new Crypto(clientUrl, clientKey, withSpeChar, clientLenght)).getPwd();
    }

    public String getPassword() {
	return this.password;
    }

}
